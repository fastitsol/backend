'use strict'

const Schema = use('Schema')

class FeedSchema extends Schema {
  up () {
    this.create('feeds', (table) => {
      table.increments()
      table.integer('user_id').unsigned()
      table.text('feedTxt', 2000).notNullable()
      table.string('feedType', 10).defaultTo('normal')
      table.string('feedPrivacy', 10).defaultTo('public')
      table.string('fileType', 10).defaultTo('image')
      table.text('fileLinks').nullable()
      table.text('metaData').nullable()

      table.timestamps()
    })
  }

  down () {
    this.drop('feeds')
  }
}

module.exports = FeedSchema
