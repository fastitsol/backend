const { validateAll } = use('Validator')
class ExampleValidator {

    createExampleRules(){
      return {
        email: 'required|email|unique:users,email',

      }
    }
    ruleMessage(){
        return {
          'firstName.required': 'Firstname is requied',
        }
    }
    async validateExampleData(data){
      return validateAll(data, this.createExampleRules(), this.ruleMessage())
    }

    async validateLoginData(data){
      return validateAll(data, {
        email: 'required|email',
        password: 'required|min:6',
      })
    }




}

module.exports = ExampleValidator
