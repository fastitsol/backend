'use strict'
const ExampleValidator = use('./ExampleValidator')
const ExampleQuery = use('./ExampleQuery')
class ExampleService {
    constructor(){
      this.exampleValidator = new ExampleValidator()
      this.exampleQuery = new ExampleQuery()
    }

    async createExample(ctx){
        const data = ctx.request.all()
        const validation = await this.exampleValidator.validateExampleData(data)
        if (validation.fails()) {
          return ctx.response.status(401).send(validation.messages())
        }
    }

}

module.exports = ExampleService
