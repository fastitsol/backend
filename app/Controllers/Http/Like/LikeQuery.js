'use strict'
const Like = use('App/Models/Like')
const Feed = use('App/Models/Feed')
class LikeQuery{
    async getFeedInfo(feed_id, uid){
      return Feed.query().where('id', feed_id).select('id','likeCount').with('like', (b)=>{
        b.where('user_id', uid)
      })
      .first()
    }
    async deleteOrCreateLike(likeData, isLike){
      if(!isLike){
          return Like.create(likeData)
       }
       return Like.query().where('user_id', likeData.user_id).where('feed_id', likeData.feed_id).delete()

    }
    async updateFeedLike(likesMeta, id){
       return Feed.query().where('id', id).update(likesMeta)
    }
}


module.exports = LikeQuery
