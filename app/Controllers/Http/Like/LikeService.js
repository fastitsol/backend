'use strict'
const LikeValidator = use('./LikeValidator')
const LikeQuery = use('./LikeQuery')
class LikeService {
    constructor(){
      this.likeValidator = new LikeValidator()
      this.likeQuery = new LikeQuery()
    }

    async createLike(ctx){
      const data = ctx.request.all()
      const validation = await this.likeValidator.validateLikeData(data)
      if (validation.fails()) {
        return ctx.response.status(401).send({message: 'Invalid data sent for likes'})
      }

      let feed = await this.likeQuery.getFeedInfo(data.feed_id,ctx.auth.user.id)
      if(!feed) return ctx.response.status(404).send({message: 'This post has been deleted!'})
      feed = feed.toJSON()
      // check if user has reacted before or not..
      if(feed.like){ // user reacted on this post before...
        feed.likeCount--
      }else{
        feed.likeCount++
      }

      Promise.all([
        this.likeQuery.deleteOrCreateLike({feed_id : data.feed_id, user_id : ctx.auth.user.id},feed.like),
        this.likeQuery.updateFeedLike({likeCount : feed.likeCount}, data.feed_id)
      ])
      return feed.likeCount

    }



}

module.exports = LikeService
