'use strict'
const LikeService = use('./LikeService')

class LikeController {

  constructor(){
      this.likeService = new LikeService()
  }

  async createLike(ctx){
      return this.likeService.createLike(ctx)
  }




}


module.exports = LikeController
