'use strict'
const Comment = use('App/Models/Comment')
const Feed = use('App/Models/Feed')
class CommentQuery{
    async createComment(data){
      return Comment.create(data)
    }
    async updateCommentCount(data, id){
       return Feed.query().where('id', id).update(data)
    }
    async getComments(id){
       return Comment.query().where('feed_id', id).limit(20).with('user').fetch()
    }
}


module.exports = CommentQuery
