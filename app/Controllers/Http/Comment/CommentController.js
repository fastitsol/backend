'use strict'
const CommentService = use('./CommentService')

class CommentController {

  constructor(){
      this.commentService = new CommentService()
  }

  async createComment(ctx){
      return this.commentService.createComment(ctx)
  }
  async getComments({params}){
     return this.commentService.getComments(params.id)
  }




}


module.exports = CommentController
