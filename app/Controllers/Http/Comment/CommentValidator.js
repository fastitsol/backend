const { validateAll } = use('Validator')
class CommentValidator {

    createCommentRules(){
      return {
        commentTxt: 'required|max:1000',
        feed_id : 'required'

      }
    }
    ruleMessage(){
        return {
          'commentTxt.required': 'Comment text is required',
          'commentTxt.max': 'Comment must be less than 1000 charecters.',
          'feed_id.required' : 'Invalid data sent for creating a comment'
        }
    }
    async validateCommentData(data){
      return validateAll(data, this.createCommentRules(), this.ruleMessage())
    }

    async validateLoginData(data){
      return validateAll(data, {
        email: 'required|email',
        password: 'required|min:6',
      })
    }




}

module.exports = CommentValidator
