'use strict'
const CommentValidator = use('./CommentValidator')
const CommentQuery = use('./CommentQuery')
const CommonQuery = use('App/Common/CommonQuery')
class CommentService {
    constructor(){
      this.commentValidator = new CommentValidator()
      this.commentQuery = new CommentQuery()
      this.commonQuery = new CommonQuery()
    }

    async createComment(ctx){
        const data = ctx.request.all()
        const validation = await this.commentValidator.validateCommentData(data)
        if (validation.fails()) {
          return ctx.response.status(401).send(validation.messages())
        }
        // check if feed exists or not
        let feed = await this.commonQuery.feedExists(data.feed_id,['id', 'commentCount'])
        if(!feed) return ctx.response.status(404).send({message: 'This post has been deleted!'})
        feed = feed.toJSON()
        const comment = await this.commentQuery.createComment({
            commentTxt : data.commentTxt,
            feed_id : data.feed_id,
            user_id : ctx.auth.user.id
          })
        const commentCount = feed.commentCount+1
        this.commentQuery.updateCommentCount({commentCount : commentCount}, data.feed_id)

        return comment

    }
    async getComments(id){
       return this.commentQuery.getComments(id)
    }

}

module.exports = CommentService
