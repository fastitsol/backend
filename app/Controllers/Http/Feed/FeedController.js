'use strict'
const FeedService = use('./FeedService')

class FeedController {

  constructor(){
      this.feedService = new FeedService()
  }

  async createFeed(ctx){
      return this.feedService.createFeed(ctx)
  }
  async getFeed(ctx){
      return this.feedService.getFeed(ctx)
  }

  parseData(){
    
  }





}


module.exports = FeedController
