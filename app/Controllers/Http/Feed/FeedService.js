'use strict'
const FeedValidator = use('./FeedValidator')
const FeedQuery = use('./FeedQuery')
class FeedService {
    constructor(){
      this.feedValidator = new FeedValidator()
      this.feedQuery = new FeedQuery()
    }
    async createFeed(ctx){
        const data = ctx.request.all()
        // let feedTxt = ''
        // const validation = await this.feedValidator.validateFeedData(data)
        // if (validation.fails()) {
        //   return response.status(401).send(validation.messages())
        // }
        if(!data.feedTxt && !data.fileType){
          return ctx.response.status(403).send({'message' : 'Please write a status or upload images or video!'})
        }
        // if(!data.feedTxt){
        //   feedTxt = 'uploaded an image'
        //   if(data.fileType == 'video'){
        //     subTxt = 'uploaded a video'
        //   }else if(data.fileType == 'gif'){
        //     subTxt = 'uploaded a gif'
        //   }else{
        //     if(data.fileLinks.length > 1){
        //       subTxt = 'uploaded images'
        //     }
        //   }

        // }

        return this.feedQuery.createFeed({
           feedTxt : data.feedTxt,
           fileType : data.fileType,
           fileLinks : data.fileLinks ? JSON.stringify(data.fileLinks) : null,
           fileFormate : data.fileFormate,
           metaData : data.metaData ? JSON.stringify(data.metaData) : null,
           feedPrivacy: data.feedPrivacy,
           user_id : ctx.auth.user.id,
           commentCount: 0,
           likeCount: 0,
           shareCount: 0


        })




    }

    async getFeed(ctx){
      let feedData = await this.feedQuery.getFeed(ctx.auth.user.id)
       feedData = feedData.toJSON()
       for(let d of feedData){
          d.fileLinks = d.fileLinks ? JSON.parse(d.fileLinks) : null
          d.comments = []
      }
       return feedData
    }


}

module.exports = FeedService
