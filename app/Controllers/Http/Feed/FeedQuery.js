'use strict'
const Feed = use('App/Models/Feed')
class FeedQuery{
    async createFeed(data){
      return Feed.create(data)
    }
    async getFeed(uid){
      return Feed.query().where('feedPrivacy', 'public').orderBy('id', 'desc')
      .with('user')
      .with('like', (b) => {
          b.where('user_id', uid)
      }).fetch()
    }

}


module.exports = FeedQuery
