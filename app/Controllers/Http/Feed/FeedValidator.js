const { validateAll } = use('Validator')
class FeedValidator {

    registerUserRules(){
      return {
        feedTxt: 'requiredIfNotExists,fileType',
        fileType: 'optional'


      }
    }
    ruleMessage(){
        return {
          'feedPrivacy': 'Please write a status or upload images or video!',
        }
    }
    async validateFeedData(data){
      return validateAll(data, this.registerUserRules(), this.ruleMessage())
    }

    async validateLoginData(data){
      return validateAll(data, {
        email: 'required|email',
        password: 'required|min:6',
      })
    }




}

module.exports = FeedValidator
