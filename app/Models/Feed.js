'use strict'

const Model = use('Model')

class Feed extends Model {
    user(){
      return this.belongsTo('App/Models/User').select('id', 'firstName', 'lastName', 'profilePic', 'isOnline')
    }
    like(){
      return this.hasOne('App/Models/Like').select('id', 'user_id', 'feed_id')
    }
}

module.exports = Feed
