'use strict'

const Model = use('Model')

class Comment extends Model {
  user(){
    return this.belongsTo('App/Models/User').select('id', 'firstName', 'lastName', 'profilePic', 'isOnline')
  }
}

module.exports = Comment
