'use strict'
const Like = use('App/Models/Like')
const Feed = use('App/Models/Feed')
class CommonQuery{
    async feedExists(feed_id, columnsToSelect){
      return Feed.query().where('id', feed_id).select(columnsToSelect).first()
    }

}


module.exports = CommonQuery
