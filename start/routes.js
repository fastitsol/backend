'use strict'

/*
|--------------------------------------------------------------------------
| Routes
|--------------------------------------------------------------------------
|
| Http routes are entry points to your web application. You can create
| routes for different URLs and bind Controller actions to them.
|
| A complete guide on routing is available here.
| http://adonisjs.com/docs/4.1/routing
|
*/

/** @type {typeof import('@adonisjs/framework/src/Route/Manager')} */

require('../app/Controllers/Http/Auth/authroute')
require('../app/Controllers/Http/Feed/feedroute')
require('../app/Controllers/Http/Like/likeroute')
require('../app/Controllers/Http/Comment/commentroute')
const Route = use('Route')

Route.get('/', () => {
  return { greeting: 'Hello world in JSON' }
})

